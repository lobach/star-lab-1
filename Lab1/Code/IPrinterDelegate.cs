namespace Lab1.Code;

public interface IPrinterDelegate : IPrinter
{
    public void Print(IPrintable printable);
}