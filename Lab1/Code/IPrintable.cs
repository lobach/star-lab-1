namespace Lab1.Code;

public interface IPrintable
{
    public void Print(IPrinter printer);
}