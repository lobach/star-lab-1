namespace Lab1.Code;

public interface IPrinter
{
    public void Print(string text);
    public void Print(char symbol);
}