namespace Lab1.Code;

public class Word : IPrintable
{
    private readonly string _word;

    public Word(string word) =>
        _word = word;

    public void Print(IPrinter printer) => 
        printer.Print(_word);
}