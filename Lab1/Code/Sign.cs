namespace Lab1.Code;

public class Sign : IPrintable
{
    private readonly char _symbol;

    public Sign(char symbol) =>
        _symbol = symbol;

    public void Print(IPrinter printer) => 
        printer.Print(_symbol);
}