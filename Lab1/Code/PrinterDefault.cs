namespace Lab1.Code;

public class PrinterDefault : IPrinterDelegate
{
    public virtual void Print(string text) => 
        Console.Write(text);

    public virtual void Print(char symbol) => 
        Console.Write(symbol);

    public void Print(IPrintable printable) => 
        printable.Print(this);
}