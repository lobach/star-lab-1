namespace Lab1.Code;

public class PrinterSpecial : PrinterDefault
{
    public override void Print(string text) =>
        base.Print($"({text})");
}