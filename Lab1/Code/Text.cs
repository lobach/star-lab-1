namespace Lab1.Code;

public class Text : IPrintable
{
    private readonly IPrintable[] _printables;

    public Text(params IPrintable[] printables) => 
        _printables = printables;

    public void Print(IPrinter printer)
    {
        foreach (var printable in _printables) 
            printable.Print(printer);
    }
}