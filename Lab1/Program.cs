﻿using Lab1.Code;

namespace Lab1;

public static class Program
{
    public static void Main(string[] args)
    {
        var txt = new Text(new Word("Тестируем"), new Sign(' '),
            new Word("мою"), new Sign(' '),
            new Word("архитектуру"), new Sign('!'));

        IPrinterDelegate printer = new PrinterSpecial();
        txt.Print(printer);
        printer.Print(txt);
    }
}